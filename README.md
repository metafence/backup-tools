# Backup tools

A small set of tools to backup and restore files

# general approach

- _setup_backup_dir.sh_ prepares a backup root directory, ie creates (if nessesary) directory, copies over all contents of directory the setup_backup_dir script is placed in and creates a empty files.lst
- _${BACKUP_ROOT}/files.lst_ contains the files and directories to be backuped, one per line and absolute path starting with /. This is to be edited by the user to tell the tool what files/directories to backup with the *next* backup.
- _${BACKUP_ROOT}/backup.sh_ iterates over files.lst and tar cjf's all entries to  ${BACKUP_ROOT}/(yy-mm-dd),  writes list of archives into file __mapping__, and copies the current version of restore.sh into that directory as well
- $BACKUP_DIR/restore.sh reads file __mapping__ , iterates over entries and restores files accordingly, preserving permissions (tar -p)

# scripts

## setup_backup_dir.sh 

usage: 

> ./setup_backup_dir.sh backup_root

- creates directory backup_root, if missing, including parents
- copies files matching  *.sh and *.md in same directory as script to backup_root
- creates an empty files.lst file in backup_root

## backup.sh

only valid when running in a backup root with a files.lst in same dir

usage: 

> ./backup.sh

- reads file.lst in the same directory as backup.sh
- creates a backup directory with directory name = $(date -I) (like yyyy-mm-dd)
- copies restore.sh to backup dir
- for each line in files.lst, tars the directory/file denoted by line into backup directory, replacing / with ~ and writing source path + target tar file to mapping file)

## restore.sh

only valid when executed in a backup dir; this makes sure that the archived files are always restorable

restore might overwrite system files so make sure to have the correct privileges. tar executes with parameter p, so should preserve file privileges (if able to)

usage:

> ./restore.sh

- for each entry in mapping file in same directory, extract corresponding tar file

# files

## $BACKUP_ROOT/files.lst

list of directories/files to be backuped. Each line results into a dedicated tar file having the name of this path, with / replaced by ~. Accepts # comments.

To be edited by the user.

Example files.lst

```
# backup syslog
/var/log/syslog

# backup cron.d
/etc/cron.d
```

## $BACKUP_DIR/mapping

contains the mapping of backuped directory/file to its tar file, one at a line.
