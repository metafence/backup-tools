#!/bin/bash

set -e

# create backup 

# inputs:
# - the directory this file is located in
# - ./files    - the list of files and directories to backup, one per line full path

SCRIPT_DIR=$(dirname $0)

echo SCRIPT_DIR is $SCRIPT_DIR

FILES_FILE=$SCRIPT_DIR/files.lst

[[ ! -e $FILES_FILE ]] && echo "files list file $FILES_FILE not found - exiting" && exit 1

BACKUP_DATE=$(date -I)
BACKUP_DIR="$SCRIPT_DIR/$BACKUP_DATE"
echo BACKUP_DIR is $BACKUP_DIR
mkdir $BACKUP_DIR || true

while read file_path ; do
	[[ $file_path =~ ^\ *#.*$ ]] && continue
	[[ $file_path =~ ^\ *$ ]] && continue
	echo processing $file_path
	archive_file=$(echo $file_path | sed -r 's!/!~!g').tbz
	archive_path=$BACKUP_DIR/$archive_file
	echo archive_path=$archive_path
	[[ -e $archive_path ]] && echo "removing already existing backup file $archive_path ... " && rm $archive_path
	tar cjvf "$archive_path" "$file_path" && echo "$archive_file $file_path" >> ${BACKUP_DIR}/mapping || true
done < $FILES_FILE

cp $SCRIPT_DIR/restore.sh $BACKUP_DIR

echo backup $BACKUP_DIR done







