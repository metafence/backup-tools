#!/bin/bash

set -e

# restore backup in this directory

# inputs:
# - the directory this file is located in
# - ./mapping    - the list of archive-to-directory mappings

STARTUP_DIR=$PWD
SCRIPT_DIR=$(readlink -f $(dirname $0))

echo SCRIPT_DIR is $SCRIPT_DIR

MAPPINGS_FILE=$SCRIPT_DIR/mapping

[[ ! -e $MAPPINGS_FILE ]] && echo "mappings file missing- this does not seem to be a backup directory - exiting" && exit 1

echo BACKUP_DIR is $SCRIPT_DIR

cd /

while read mapping ; do
	echo processing mapping $mapping ...
	archive_path=$(echo $mapping | cut -d ' ' -f 1)
	echo archive_path=$archive_path
	target_path=$(echo $mapping | cut -d ' ' -f 2)
	echo target_path=$target_path
	tar xvpf $SCRIPT_DIR/$archive_path
done < $MAPPINGS_FILE

cd $STARTUP_DIR






