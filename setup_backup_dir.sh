#!/bin/bash

set -e

# bootstrap directory to be usable by backupr 

[[ -z $1 ]] && echo "
$0 <backupRootDir> initialize directory to be usable as a backup directory" && exit 1

SCRIPT_DIR=$(dirname $0)
echo SCRIPT_DIR is $SCRIPT_DIR

BACKUP_DIR=$1

mkdir -pv $BACKUP_DIR || true

cp -v $SCRIPT_DIR/*.sh $BACKUP_DIR
cp -v $SCRIPT_DIR/*.md $BACKUP_DIR
touch $BACKUP_DIR/files.lst
echo Done







